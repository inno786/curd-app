import React from 'react';
import PropsType from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import CustomButton from '../../../Components/Button/CustomButton';

const ListItem = ({ item, onDelete }) => (
    <tr key={item._id}>
        <td>{item._id}</td>
        <td>{`${item.fname} ${item.lname}`}</td>
        <td>{item.email}</td>
        <td>{moment(item.dob).format('YYYY-MM-DD')}</td>
        <td>{item.designation}</td>
        <td>{item.status ? 'Active' : 'Inactive'}</td>
        <td>
            <Link to={`/user/update/${item._id}`}><CustomButton type="button" displayText="Update" /></Link>
            <CustomButton type="button" clickHandler={() => onDelete(item._id)} displayText="Delete" />
        </td>
    </tr>
);

// Props Validation
ListItem.propsType = {
    item: PropsType.object.isRequired,
    onDelete: PropsType.func.isRequired
}
export default ListItem;