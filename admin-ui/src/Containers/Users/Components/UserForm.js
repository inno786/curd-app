import React from 'react';
import PropTypes from 'prop-types';
import style from './Style'

import { Panel, Form, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';

const UserForm = props => (
    <div style={style.divStyle}>
        <div style={style.error}>{props.msg}</div>
        <Panel style={style.panelStyle}>
            <Form onSubmit={props.onSubmit}>
                <FormGroup controlId="fname">
                    <ControlLabel>First Name</ControlLabel>
                    <FormControl
                        type="text"
                        placeholder="First Name"
                        name="fname"
                        value={props.formData.fname}
                        onChange={props.onChange}
                    />
                    {props.errors.fname && <ControlLabel className="error">{props.errors.fname}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="lname">
                    <ControlLabel>Last Name</ControlLabel>
                    <FormControl
                        type="text"
                        placeholder="Last Name"
                        name="lname"
                        value={props.formData.lname}
                        onChange={props.onChange}
                    />
                    {props.errors.lname && <ControlLabel className="error">{props.errors.lname}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="email">
                    <ControlLabel>Email</ControlLabel>
                    <FormControl
                        type="email"
                        name="email"
                        value={props.formData.email}
                        placeholder="Email"
                        onChange={props.onChange}
                    />
                    {props.errors.email && <ControlLabel className="error">{props.errors.email}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="password">
                    <ControlLabel>Password</ControlLabel>
                    <FormControl
                        type="password"
                        name="password"
                        value={props.formData.password}
                        placeholder="Password"
                        onChange={props.onChange}
                    />
                    {props.errors.password && <ControlLabel className="error">{props.errors.password}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="formDOB">
                    <ControlLabel>DOB</ControlLabel>
                    <FormControl
                        type="date"
                        name="dob"
                        value={props.formData.dob}
                        placeholder="YYYY-MM-DD"
                        onChange={props.onChange}
                    />
                    {props.errors.dob && <ControlLabel className="error">{props.errors.dob}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="formControlsSelectDesignation">
                    <ControlLabel>Select Designation</ControlLabel>
                    <FormControl
                        componentClass="select"
                        name="designation"
                        value={props.formData.designation}
                        placeholder="Select Designation"
                        onChange={props.onChange}
                    >
                        <option value="">Select Designation</option>
                        {props.designation.length && props.designation.map((item, idx) => <option key={idx} value={item.value}>{item.label}</option>)}
                    </FormControl>
                    {props.errors.designation && <ControlLabel className="error">{props.errors.designation}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="formControlsSelectStatus">
                    <ControlLabel>Select Status</ControlLabel>
                    <FormControl
                        componentClass="select"
                        placeholder="Select Status"
                        name="status"
                        value={props.formData.status}
                        onChange={props.onChange}
                    >
                        <option value="">Select Status</option>
                        <option value={1}>Active</option>
                        <option value={0}>Inactive</option>
                    </FormControl>
                    {props.errors.status && <ControlLabel className="error">{props.errors.status}</ControlLabel>}
                </FormGroup>
                <FormGroup style={style.buttonStyle} controlId="formSubmit">
                    <Button bsStyle="primary" type="submit"> Create</Button>
                </FormGroup>
            </Form>
        </Panel>
    </div>
);

UserForm.propTypes = {
    formData: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    designation: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
}
export default UserForm;