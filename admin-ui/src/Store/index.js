import { applyMiddleware, createStore } from 'redux';
import createLogger from 'redux-logger'
import thunk from 'redux-thunk'
import rootReducers from './Reducers'

const middleware = [thunk]
middleware.push(createLogger)

const store = createStore(
    rootReducers,
    applyMiddleware(...middleware)
);

export default store
