const dateFormat = require('dateformat');
const randomstring = require("randomstring");
const User = require('mongoose').model('User');
const joiValidation = require('../validation/user');
const encryption = require('../lib/encryption');

let create = async (req, res, next) => {
    let validation = await joiValidation.validateUserCreate({ ...req.body, code: randomstring.generate(32) });

    if (validation.error) return res.status(200).json({ error: validation.error, message: '' });

    const now = new Date();
    const datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
    const objUser = new User({
        ...validation.value,
        password: encryption.encrypt(validation.value.password, validation.value.code),
        createdAt: datetime,
        updatedAt: datetime
    });

    objUser.save((error, data) => {
        if (error) {
            if (error.code && error.code === 11000) return res.status(200).json(
                { error: [{ key: 'email', value: 'Email Address is already taken.' }], message: '' }
            );
            return res.status(200).json(
                { error, message: error.message || 'Internal server error!' }
            );
        }
        return res.status(200).json({ error: null, data });
    });
};
let remove = (req, res, next) => {
    User.findOneAndDelete({ _id: req.headers._id }, async (error, data) => {
        if (error) return res.status(200).json({ error });
        const response = await findAllUser();
        return res.status(200).json(response);
    });
};
let login = async (req, res, next) => {
    let validation = await joiValidation.validateLogin(req.body);

    if (validation.error) return res.status(200).json({ error: validation.error, message: '' });

    let findUserByEmail = await findUserByAttributes({ email: validation.value.email });

    if (findUserByEmail.error || !findUserByEmail.data) return res.status(200).json(
        {
            error: [{ key: 'email', value: 'Incorrect email address' }, { key: 'password', value: 'Incorrect password' }],
            message: 'Incorrect Email/Password, Please try again!'
        });

    if (encryption.decrypt(findUserByEmail.data.password, findUserByEmail.data.code) !== validation.value.password) return res.status(200).json(
        {
            error: [{ key: 'email', value: '' }, { key: 'password', value: 'Incorrect password' }],
            message: ''
        });
    return res.status(200).json({ error: null, message: 'Logging successfull.' });
};

let list = async (req, res, next) => {
    const response = await findAllUser();
    return res.status(200).json(response);
};


let findUserByAttributes = condition => {
    return new Promise(resolve => {
        User.findOne(condition, (error, data) => {
            if (error) return resolve(error);
            if (data) return resolve({ error: null, data });
            return resolve({ error: null, data: null });
        });
    });
};
let findAllUser = () => {
    return new Promise(resolve => {
        User.find({}, (error, data) => {
            if (error) return resolve({ error, data: [] });
            if (data) return resolve({ error: null, data });
        });
    });
};
module.exports = { create, remove, login, list };